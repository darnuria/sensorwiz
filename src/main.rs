

extern crate log;

extern crate uuid;

extern crate env_logger;
extern crate clap;
extern crate futures;
extern crate tokio;

mod mqtt;

use log::{info, trace, warn, error};

use std::io::Write;
use std::fmt::Debug;
use std::env;
use std::net;
use std::str;
use std::time::{Duration, Instant};

use clap::{App, Arg};

use uuid::Uuid;

use serde::{Deserialize, Serialize};
//use serde_json;
use chrono;

use futures::{future, Future, Stream};

use actix_web::{server, Path, Responder, http, HttpResponse, HttpRequest, State};
use actix::prelude::*;
use tokio::io::{self, AsyncRead};
use tokio::timer::Interval;

use database;

/* TODO Candy:
   - Command line
   -
*/

//extern crate dotenv;

// Diesel - actix
// Wrapping diesel into a synchronous actor.

// mod {
// use actix::prelude::*;

// struct DbExecutor(DatabaseConnection);

// impl Actor for DbExecutor {
//     type Context = SyncContext<Self>;
// }

// struct CreateLuminosity {
//     board_id: i32,
//     val: f64,
// }

// impl Handler<CreateUser> for DbExecutor {
//     type Result = Result<User, Error>;

//     fn handle(&mut self, msg: CreateLuminosity, _: &mut Self::Context) -> Self::Result
//     {
//         let db = &self.0;
//         database::sensors::create_luminosity(db, msg.board_id, val, chrono::Utc::now());
        
//         // Create insertion model
//         let uuid = format!("{}", uuid::Uuid::new_v4());
//         let new_user = models::NewUser {
//             id: &uuid,
//             name: &msg.name,
//         };

//         let mut items = users
//             .filter(id.eq(&uuid))
//             .load::<models::User>(&self.0)
//             .expect("Error loading person");

//         Ok(items.pop().unwrap())
//     }
// }

// pub struct AppState {
//     db
// } 

// Diesel

struct AppState {
    mqtt: Addr<mqtt::MqttExecutor>,
//        db  : Addr<DbExecutor>
}


/// simple index handler

fn luminosity(
(req, state): (HttpRequest<AppState>, State<AppState>))
-> HttpResponse 
{
    println!("{:?}", req);

    // response
    HttpResponse::Ok()
        .content_type("text/json; charset=utf-8")
        .body("Hello")
}

//use MqttExecutor;
// Arch roadmap:
//  Main thread: Console, management of faillure
// other threads
//  - (wrap it in actix?) IO thread + MQTT
//  - actix web?
//  - Database thread
fn main() {

    // configure logging
    env::set_var("RUST_LOG", env::var_os("RUST_LOG").unwrap_or_else(|| "info".into()));
    env_logger::init();

     let matches = App::new("datawizz")
        .author("Axel Viala <darnuria@darnuria.eu>")
    //    .arg(
        //     Arg::with_name("MQTT_SERVER")
        //         .short("S")
        //         .long("mqtt-server")
        //         .takes_value(true)
        //         .required(true)
        //         .help("MQTT server address (host:port)")
        // )
        // .arg(
        //     Arg::with_name("DATABASE_SERVER")
        //         .short("D")
        //         .long("database-server")
        //         .takes_value(true)
        //         .required(true)
        //         .help("Database url ex: postgres://user:password@localhost/datawiz"))
        .get_matches();

    // let database_url = matches.value_of("DATABASE_SERVER")
    //     .expect("DATABASE_URL must be set");

    let database_url = "postgres://datawiz:hello@localhost/datawiz";

    let db = database::establish_connection(&database_url);
    // Apply migration on new installations.
    
    database::apply_migrations(&db).expect("Oh noes migration is going wild, I see pink elephants!");

    // debug = "127.0.0.1:1883";
    // let mqtt_server_addr = matches.value_of("MQTT_SERVER").unwrap();
    // let client_id = generate_client_id();

    // let keep_alive = 10;
    // let dbAddr = SyncArbiter::start(3, move || DbExecutor(pool.clone()));
    
    let mqttAddr = mqtt::MqttExecutor.start();
    let system = actix::System::new("sensor-wizz");


    server::new(move || {
    actix_web::App::with_state(AppState {
        //db: dbAddr.clone(),
        mqtt: mqttAddr.clone()
    }).resource("/luminosity", |r| r.method(http::Method::GET).with(luminosity))
    }).bind("127.0.0.1:8081").unwrap()
      .run();


    let _ = system.run();
}