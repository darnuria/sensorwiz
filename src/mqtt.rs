// Imported from https://github.com/loggi/actix-tools/blob/master/src/mqtt.rs
// Author @gabisurita
use actix::prelude::*;
pub use rumqtt::{Message as MqttMessage, MqttClient, MqttOptions, QoS};

// JSON from mqtt

use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize, Debug)]
struct SensorData {
    val: f64,
}

#[derive(Clone, Deserialize)]
pub struct MqttSettings {
    pub addr:          String,
    pub port:          u16,
    pub keep_alive:    usize,
//    pub reconnect:     usize,
    pub clean_session: bool,
    pub client_id:     String,
}

impl Into<MqttOptions> for MqttSettings {
    fn into(self) -> MqttOptions {
        MqttOptions::new(self.client_id, self.addr, self.port)
//            .set_reconnect(self.reconnect as u16)
            .set_keep_alive(self.keep_alive as u16)
            .set_clean_session(self.clean_session)
    }
}

pub struct MqttExecutor;

pub fn settings() -> MqttSettings {
    MqttSettings {
        addr: "127.0.0.1".to_string(),
        port: 1883,
        keep_alive: 60,
//            reconnect: 60,
        clean_session: false,
        client_id: "sensorwiz".to_string(),
    }
}

pub fn subscriptions() -> Vec<(String, QoS)> {
    vec![
        ("esp32/1/luminosity".to_string(), QoS::ExactlyOnce ),
    ]
}




impl Actor for MqttExecutor {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Context<Self>) {
        let addr = ctx.address();

        let (mut client, notifications) = MqttClient::start(
            settings().into()
        ).unwrap();

        subscriptions()
            .iter()
            .for_each(| (topic, qos) |
            client.subscribe( topic.as_str(), *qos).expect("Something with mqtt subscription went wrong.")
        );

        for notif in notifications {
            println!("Whouah look at da message {:?}", notif);
        }
    }

    fn stopped(&mut self, ctx: &mut Context<Self>) {
       println!("Actor is stopped");
    }

}
