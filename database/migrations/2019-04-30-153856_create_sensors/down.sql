-- This file should undo anything in `up.sql`
drop table luminosity;
drop table sensors;
drop table boards;
drop type si_unit;
