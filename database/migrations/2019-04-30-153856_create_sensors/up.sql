-- This migration add minimal info about sensors on which board they are
-- and which captor they embbed.


create table boards (
    id serial primary key,
    title varchar not null
);

-- See https://crates.io/crates/diesel-derive-enum
-- create type si_unit as enum ('lux', 'celcius');

create table sensors (
  id serial primary key,
--  unit si_unit not null,
  title varchar not null,
  board_id integer references boards(id) not null
);

create table luminosity (
  id serial primary key,
  sensor_id integer references sensors(id) not null,
  val double precision not null,
  created timestamp with time zone not null
);