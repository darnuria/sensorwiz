table! {
    boards (id) {
        id -> Int4,
        title -> Varchar,
    }
}

table! {
    luminosity (id) {
        id -> Int4,
        sensor_id -> Int4,
        val -> Float8,
        created -> Timestamptz,
    }
}

table! {
    sensors (id) {
        id -> Int4,
        title -> Varchar,
        board_id -> Int4,
    }
}

joinable!(luminosity -> sensors (sensor_id));
joinable!(sensors -> boards (board_id));

allow_tables_to_appear_in_same_query!(
    boards,
    luminosity,
    sensors,
);
