use diesel::prelude::*;
// use diesel::result::Error;
// use diesel::sql_types::{Integer, Text};
// use lazy_static::lazy_static;
// use regex::Regex;

use crate::models::*;
use crate::{ DatabaseError, DbConnection };

use crate::models::{ Board, NewBoard, NewSensor, NewLuminosity };
use chrono::{DateTime, Utc};


// Note: now create_* function don't manage error...
// They are checked in the client.
pub fn create_board<'a>(db: &DbConnection, title: &'a str) -> Board {
  use crate::schema::boards;

  let new_board = NewBoard {
    title,
  };

  diesel::insert_into(boards::table)
    .values(&new_board)
    .get_result(db)
    .expect("Sorry I my dear friend the database, is unable to insert a new board at the moment.")
}

pub fn create_sensors<'a>(db: &DbConnection, title: &'a str, board_id: i32) -> Sensor {
  use crate::schema::sensors;

  let s = NewSensor {
    title,
    board_id
  };

  diesel::insert_into(sensors::table)
    .values(&s)
    .get_result(db)
    .expect("Sorry I my dear friend the database, is unable to insert a new sensor at the moment.")
}

pub fn create_luminosity(db: &DbConnection, sensor_id: i32, val: f64, created: DateTime<Utc>) -> Luminosity {
	use crate::schema::luminosity;

	let l = NewLuminosity {
		sensor_id,
		val,
		created,
	};

	diesel::insert_into(luminosity::table)
		.values(&l)
		.get_result(db)
		.expect("Sapristy something went wrong while inserting a luminosity")
}

pub trait BoardQueryable {
    fn boards_all(db: &DbConnection) -> Result<Vec<Board>, DatabaseError>;
}

pub struct BoardQuery;

impl BoardQueryable for BoardQuery {
    fn boards_all(db: &DbConnection) -> Result<Vec<Board>, DatabaseError> {
        use crate::schema::boards::dsl::*;

        boards
          .order_by(id)
          .load(db)
          .map_err(|e| DatabaseError::Other { cause: e })
    }
}

pub struct SensorsQuery;

pub trait SensorQueryable {
    fn luminosity_all(db: &DbConnection) -> Result<Vec<Luminosity>, DatabaseError>;
    fn sensors_all(db: &DbConnection) -> Result<Vec<Sensor>, DatabaseError>;
}

impl SensorQueryable for SensorsQuery {
    fn luminosity_all(db: &DbConnection) -> Result<Vec<Luminosity>, DatabaseError> {
        use crate::schema::luminosity::dsl::*;

        luminosity
          .order_by(created)
          .load(db)
          .map_err(|e| DatabaseError::Other { cause: e })
    }

    fn sensors_all(db: &DbConnection) -> Result<Vec<Sensor>, DatabaseError> {
        use crate::schema::sensors::dsl::*;

        sensors
          .order_by(id)
          .load(db)
          .map_err(|e| DatabaseError::Other { cause: e })
    }
}


#[cfg(test)]
mod tests {
    use std::io::stdout;
    use std::path::Path;

    use diesel_migrations::run_pending_migrations_in_directory;

    use super::*;
    use crate::establish_connection;

    #[test]
    fn all() {
        let conn = {
            let conn = establish_connection("postgres://datawiz:hello@localhost/datawiz");
            run_pending_migrations_in_directory(&conn, &Path::new("./migrations"), &mut stdout())
                .unwrap();
            conn
        };

        conn.test_transaction::<_, DatabaseError, _>(|| {
        {
            let b_name = "test";
            let ack = create_board(&conn, b_name);
            let result = BoardQuery::boards_all(&conn)?;
            assert_eq!(result.len(), 1);
            assert_eq!(result[0].title, b_name);

            {
                let s_name = "Nioutaik 300";
                let ack = create_sensors(&conn, s_name, result[0].id);
                let result = SensorsQuery::sensors_all(conn);
                assert_eq!(result.len(), 1);
                assert_eq!(result[0].title, s_name);
            }
        }


            // // Book
            // {
            //     let result = SwordDrill::book("psa", &conn)?;

            // assert_eq!(result.0.name, "Psalms");
            // assert_eq!(result.1.len(), 150);
            // }

            // // All books
            // {
            //     let result = SwordDrill::all_books(&conn)?;

            // assert_eq!(result.len(), 66);
            // assert_eq!(result[64].name, "Jude");
            // }

            // // Search - Fuzzy words
            // {
            //     let result = SwordDrill::search("fire hammer rock", &conn)?;

            //     assert_eq!(result.len(), 1);
            //     assert_eq!(result[0].0.book, 24);
            //     assert_eq!(result[0].0.chapter, 23);
            //     assert_eq!(result[0].0.verse, 29);
            //     assert_eq!(
            //         result[0].0.words,
            //         "Is not my word like as a <em>fire</em>? saith the LORD; and like a <em>hammer</em> that breaketh the <em>rock</em> in pieces?",
            //     );
            //     assert_eq!(result[0].1.name, "Jeremiah");
            // }



            // // Search - Phrase
            // {
            //     let result = SwordDrill::search("\"like as a fire\"", &conn)?;

            //     assert_eq!(result.len(), 1);
            //     assert_eq!(result[0].0.book, 24);
            //     assert_eq!(result[0].0.chapter, 23);
            //     assert_eq!(result[0].0.verse, 29);
            //     assert_eq!(
            //         result[0].0.words,
            //         "Is not my word <em>like as a fire</em>? saith the LORD; and like a hammer that breaketh the rock in pieces?",
            //     );
            //     assert_eq!(result[0].1.name, "Jeremiah");
            // }
            Ok(())
        });
    }
}
