
#![allow(proc_macro_derive_resolution_fallback)]
//use diesel::backend::Backend;
// use diesel::deserialize::{ Queryable, };
//use serde_derive::{Deserialize, Serialize};
use chrono::{DateTime, Utc};

#[derive(Queryable)]
pub struct Sensor {
    pub id: i32,
    pub title: String,
    pub board_id: i32
}

#[derive(Queryable)]
pub struct Board {
    pub id: i32,
    pub title: String
}

use crate::schema::boards;
#[derive(Insertable)]
#[table_name="boards"]
pub struct NewBoard<'a> {
    pub title: &'a str,
}

use crate::schema::sensors;
#[derive(Insertable)]
#[table_name="sensors"]
pub struct NewSensor<'a> {
    pub title: &'a str,
    pub board_id: i32
}

#[derive(Queryable)]
pub struct Luminosity {
    pub id: i32,
    pub sensor_id: i32,
    pub val: f64,
    pub created: DateTime<Utc>
}

use crate::schema::luminosity;
#[derive(Insertable)]
#[table_name="luminosity"]
pub struct NewLuminosity {
    pub val: f64,
    pub sensor_id: i32,
    pub created: DateTime<Utc>
}
