#![allow(proc_macro_derive_resolution_fallback)]
#[macro_use] extern crate diesel;

pub mod schema;
pub mod models;

pub mod sensors;

use std::io::stdout;
use std::path::Path;

use diesel::prelude::*;
// use diesel::r2d2;
use diesel::result::Error;
use diesel_migrations::{
    connection::MigrationConnection, run_pending_migrations_in_directory, RunMigrationsError,
};


use failure::Fail;

pub type DbConnection = PgConnection;

#[derive(Fail, Debug)]
pub enum DatabaseError {
//     #[fail(display = "'{}' was not found.", book)]
//     BookNotFound { book: String },

//     #[fail(display = "There was a connection pool error.")]
//     ConnectionPool { cause: String },

    #[fail(display = "I encountered an misenterstanding when talking to my fellow database friend. Here what he said: {:?}.", cause)]
    Other { cause: Error },

    #[fail(
        display = "My friend the database got an indigestion with the last migration please help him. Root cause: {:?}.",
        cause
    )]
    Migration { cause: RunMigrationsError },
} // DatabaseError


pub fn establish_connection(pg_url: &str) -> PgConnection {
    // dotenv().ok();

    // let database_url = env::var("DATABASE_URL")
    //     .expect("DATABASE_URL must be set");
    PgConnection::establish(pg_url)
        .expect(&format!("Something went wrong with the connection to the database at: <{}>", pg_url))
}


/// Run any pending Diesel migrations.
pub fn apply_migrations<Conn>(conn: &Conn) -> Result<(), DatabaseError>
where
  Conn: MigrationConnection,
{
    let folder = Path::new("./database/migrations");
    run_pending_migrations_in_directory(conn, &folder, &mut stdout())
        .map_err(|e| DatabaseError::Migration { cause: e })
}