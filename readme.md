## Setting up postgresql

### Diesel setup

```bash
diesel setup
```

### creating role

```sql
create role datawiz SUPERUSER CREATEDB CREATEROLE \
  with login encrypted password 'hello';
```

## Test application

```bash
datawiz --database-server "postgres://datawiz:hello@localhost/datawiz"
```